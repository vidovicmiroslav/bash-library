# Usage: check_existence [<package> ...]
#
# Check existence of apps or packages that are required in a bash script. If any
# one of the required packages is not available the script is terminated with 
# exit 1.
#
# Example:
#
#   check_existence youtube-dl subliminal jq


check_existence(){
  local requirements=("$@")
  for app in "${requirements[@]}"; do
    type "$app" >/dev/null 2>&1 || \
      { echo >&2 "$app is required but it's not installed. Aborting."; exit 1; }
  done
}


# Usage: check_input_args
#
# Check if no positional arguments are supplied to the script. If no variable is
# available ($# is 0) the script is terminated with exit 1.
#


check_input_args(){
  if [ "$#" -eq 0 ]
  then
    echo "No arguments supplied"
  fi
}


# Usage: check_variable <var>
#
# Check if a variable is a null string and terminate the script.
#
# Example:
#   check_variable $VAR


check_variable(){
  local var="$1"
  if [ -z "$var" ]
  then
    echo "Variable not set"
    exit 1
  fi
}
